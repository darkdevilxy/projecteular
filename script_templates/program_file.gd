extends Node2D
signal output(output_text)

func _ready() -> void:
	
	
	var output_text:String = ""
	emit_signal("output", output_text)
	pass

