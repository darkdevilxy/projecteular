extends Node2D
signal output(output_text)
signal console_setting_changed(setting, key_value)
signal script_generated(text)

var expression_text:String

var Consle_Settings:Dictionary =  {
	readonly = 'console.set("readonly", true)',
	set_focus_none = 'console.set_focus_mode(0)'
}

func _ready() -> void:
	pass

func script_generated(text:String) -> void:
	emit_signal("script_generated", text)

# refrences the console so it can be used form anywhere in the script
func console_output(output_text) -> void:
	output_text = str(output_text)
	emit_signal("output", output_text)
	pass

# refrenced when a script need to make some changes to the console settings
func console_settings_changed(changed_setting: Array) -> void:
	emit_signal("console_setting_changed", changed_setting)
